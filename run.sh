#!/usr/bin/env bash

set -e

OUTPUT_BASE_DIR=${OUTPUT_BASE_DIR:-./data}
DAY_INDEX="$(( "$(date +%H)" / 12 ))" # 0: 0-11; 1: 12-23
OUTPUT_DIR="${OUTPUT_BASE_DIR}/$(date +%Y-%m-%d)-${DAY_INDEX}"

echo "+ output folder: ${OUTPUT_DIR}"

if [[ ! -d "${OUTPUT_DIR}" ]]; then
  mkdir -p "${OUTPUT_DIR}"
fi

# + grep -v "^$" removes empty lines
# + mapfile -t DBS -> read file into array (each line as an element)
# + <() -> create a temporary file and execute the command
mapfile -t DBS < <(psql -c "SELECT datname FROM pg_database WHERE datistemplate = false" -t | grep -v "^$")

#printf "🍺  %s\n" "${DBS[@]}"

for DB in "${DBS[@]}"; do
  DB=${DB// /} # remove spaces
  echo -n "> ${DB} "

  # start dumping DB
  # -O: skip restoration of object ownership in plain-text format
  # -x: do not dump privileges
  # -n public: only dump public schema to avoid error on PostGIS
  # -Fc: to pipeline output as binary format
  pg_dump -E UTF8 -O -x -Fc "${DB}" > "${OUTPUT_DIR}/${DB}.sql.gz"  
  echo -e "✅"
done
